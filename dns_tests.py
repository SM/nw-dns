#!/usr/bin/env python3

"""Tests for your DNS resolver and server"""


import sys
import unittest
from unittest import TestCase
from argparse import ArgumentParser
import socket
from dns.types import *
from dns.classes import *
from dns.name import *
from dns.message import *
from dns.resolver import *
from dns.resource import *
import time


PORT = 5001
SERVER = "localhost"


class TestResolver(TestCase):

    def invalid_cache(self):
        resolver = Resolver(10, True, 5)
        hostname, aliaslist, iplist = resolver.gethostbyname("invalid_domain.asdf")
        self.assertEqual(hostname, "invalid_domain.asdf")
        self.assertEqual(iplist[0],"111.0.111.0")

    def short_ttl(self):
        cache = RecordCache()
        cache.read_cache_file()
        cache.add_record(ResourceRecord("invalid_domain2.asdf.",Type.A, Class.IN, 10, ARecordData("111.0.111.1")))
        resolver = Resolver(10, True, 5)
        time.sleep(11)
        hostname, aliaslist, iplist = resolver.gethostbyname("invalid_domain2.asdf")
        self.assertEqual(iplist.__len__(), 0)

class TestCache(TestCase):
    """Cache tests"""


class TestResolverCache(TestCase):
    """Resolver tests with cache enabled"""


class TestServer(TestCase):

    def test_inside_zone(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        question = Question(Name("test1.steffan-mark.nl"), Type.A, Class.IN)
        header = Header(3, 0, 1, 0, 0, 0)
        header.qr = 0
        header.opcode = 0
        header.rd = 0
        query = Message(header, [question])
        sock.sendto(query.to_bytes(), ("127.0.0.1", 53))
        data = sock.recv(512)
        message = Message.from_bytes(data)
        self.assertEqual(message.answers[0].rdata.address, "10.0.0.1")

    def test_referral(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        question = Question(Name("mark-steffan.nl"), Type.A, Class.IN)
        header = Header(4, 0, 1, 0, 0, 0)
        header.qr = 0
        header.opcode = 0
        header.rd = 0
        query = Message(header, [question])
        sock.sendto(query.to_bytes(), ("127.0.0.1", 53))
        data = sock.recv(512)
        message = Message.from_bytes(data)
        self.assertEqual(message.authorities[0].rdata.nsdname.__str__(), "ns.mark-steffan.nl.")

    def test_outside_zone(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        question = Question(Name("ru.nl"), Type.A, Class.IN)
        header = Header(5, 0, 1, 0, 0, 0)
        header.qr = 0
        header.opcode = 0
        header.rd = 1
        query = Message(header, [question])
        sock.sendto(query.to_bytes(), ("127.0.0.1", 53))
        data = sock.recv(512)
        message = Message.from_bytes(data)
        self.assertEqual(message.answers[0].rdata.address, "131.174.78.60")

    def test_outside_zone_no_rec(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        question = Question(Name("ru.nl"), Type.A, Class.IN)
        header = Header(5, 0, 1, 0, 0, 0)
        header.qr = 0
        header.opcode = 0
        header.rd = 0
        query = Message(header, [question])
        sock.sendto(query.to_bytes(), ("127.0.0.1", 53))
        data = sock.recv(512)
        message = Message.from_bytes(data)
        self.assertEqual(message.header.an_count, 0)


def run_tests():
    """Run the DNS resolver and server tests"""
    parser = ArgumentParser(description="DNS Tests")
    parser.add_argument("-s", "--server", type=str, default="localhost",
                        help="the address of the server")
    parser.add_argument("-p", "--port", type=int, default=5001,
                        help="the port of the server")
    args, extra = parser.parse_known_args()
    global PORT, SERVER
    PORT = args.port
    SERVER = args.server

    # Pass the extra arguments to unittest
    sys.argv[1:] = extra

    # Start test suite
    unittest.main()


if __name__ == "__main__":
    run_tests()
