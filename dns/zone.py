#!/usr/bin/env python3

"""Zones of domain name space

See section 6.1.2 of RFC 1035 and section 4.2 of RFC 1034.
Instead of tree structures we simply use dictionaries from domain names to
zones or record sets.

These classes are merely a suggestion, feel free to use something else.
"""

from dns.resource import *
from dns.classes import *
from dns.name import *
from dns.types import *

class Catalog:
    """A catalog of zones"""

    def __init__(self):
        """Initialize the catalog"""
        self.zones = {}

    def add_zone(self, name, zone):
        """Add a new zone to the catalog

        Args:
            name (str): root domain name
            zone (Zone): zone
        """
        self.zones[name] = zone


class Zone:
    """A zone in the domain name space"""

    def __init__(self):
        """Initialize the Zone """
        self.records = {}

    def lookupwithtype(self, name, type_):
        if not isinstance(name, Name):
            name = Name(name)

        result = []
        while True:
            if name.labels.__len__() == 0:
                return name, result

            zonedata = self.records.get(name.__str__())
            if zonedata is None:
                return self.lookupwithtype(Name(name.labels[1:]), Type.NS)
            else:
                for record in zonedata:
                    if record.type_ == Type.CNAME:
                        return name, [record]
                    if record.type_ == type_:
                        result.append(record)
                return name, result


    def lookup(self, name):
        if not isinstance(name, Name):
            name = Name(name)

        result = []
        while True:
            if name.labels.__len__() == 0:
                return name, result

            zonedata = self.records.get(name.__str__())
            if zonedata is None:
                return self.lookupwithtype(Name(name.labels[1:]), Type.NS)
            else:
                for record in zonedata:
                    result.append(record)
                return name, result


    def add_node(self, name, record_set):
        """Add a record set to the zone

        Args:
            name (str): domain name
            record_set ([ResourceRecord]): resource records
        """
        if not isinstance(name, Name):
            name = Name(name)

        name = name.__str__()
        if self.records.get(name) is None:
            self.records[name] = record_set
        else:
            self.records[name] = self.records[name] + record_set

    def read_master_file(self, filename):
        """Read the zone from a master file

        See section 5 of RFC 1035.

        Args:
            filename (str): the filename of the master file
        """

        classdict = {
            Type.A: ARecordData,
            Type.CNAME: CNAMERecordData,
            Type.NS: NSRecordData
        }

        try:
            with open(filename, "r") as file:
                for line in file:
                    indexofc = line.find(';')  # Check if there is an ; in the line
                    if indexofc == -1:
                        words = line.split()
                    else:
                        words = line[0:indexofc].split()

                    type_ = Type[words[2]]
                    if type_ not in classdict:
                        raise ValueError('Zone contains unknown type!')

                    rdata = classdict[type_](words[3])
                    rr = ResourceRecord(Name(words[0]), type_, Class.IN, int(words[1]), rdata)
                    self.add_node(words[0], [rr])
        except ValueError as e:
            print(repr(e))
        except:
            print("could not read zone from master file")