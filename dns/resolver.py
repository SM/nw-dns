#!/usr/bin/env python3

"""DNS Resolver

This module contains a class for resolving hostnames. You will have to implement
things in this module. This resolver will be both used by the DNS client and the
DNS server, but with a different list of servers.
"""


import socket

from dns.classes import Class
from dns.message import Message, Question, Header
from dns.name import Name
from dns.types import Type
from dns.cache import RecordCache
from dns.resource import ResourceRecord


class Resolver:
    """DNS resolver"""

    def __init__(self, timeout, caching, ttl):
        """Initialize the resolver

        Args:
            caching (bool): caching is enabled if True
            ttl (int): ttl of cache entries (if > 0)
        """
        self.timeout = timeout
        self.caching = caching
        self.ttl = ttl

        # Set initial server list with root DNS-servers
        self.serverlist = [
            "193.0.14.129",     # K root server
            "202.12.27.33"      # M root server
        ]

        self.aliaslist = []

        # Initialize cache
        self.cache = RecordCache(ttl)
        self.cache.read_cache_file()


    def gethostbyname(self, hostname):
        """Translate a host name to IPv4 address.

        Currently this method contains an example. You will have to replace
        this example with the algorithm described in section 5.3.3 in RFC 1034.

        Args:
            hostname (str): the hostname to resolve

        Returns:
            (str, [str], [str]): (hostname, aliaslist, ipaddrlist)
        """



        # Algorithm as described in RFC 1034

        serverlist = self.serverlist.copy()

        # Step 1: Check cache
        if self.caching:
            print("using cache...")
            while True:
                cacheresult = self.resolvehostnameincache(Name(hostname))
                if cacheresult.__len__() > 0:
                    if isinstance(cacheresult[0], ResourceRecord):
                        if cacheresult[0].type_ == Type.CNAME:
                            print("CNAME")
                            hostname = cacheresult[0].rdata.cname.__str__()
                        else:
                            print("Directly from cache")
                            aliaslist, ipaddrlist = self.getresponselists(cacheresult, hostname)
                            # Store current state of cache in cache file
                            self.cache.write_cache_file()
                            return hostname, aliaslist, ipaddrlist
                    else:
                        # Ip addresses for more-than-root-specific name servers found
                        for ip in cacheresult:
                            serverlist.insert(0, ip)
                        break
                else:
                    # Nothing found in cache
                    break


        # Step 2: Find best server to ask
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(self.timeout)
        response = None

        while serverlist.__len__() > 0:
            response = self.sendrequestmessage(sock, Name(hostname), serverlist.pop(0))
            if (response.answers.__len__() > 0) | (response.header.rcode == 0b0011):
                # Store result in cache
                # and check if one of the answers is a CNAME RR
                cnamefound = False
                for answer in response.answers:
                    # Store result in cache
                    self.cache.add_record(answer)

                    if (answer.type_ == Type.CNAME) & (cnamefound is False):
                        cnamefound = True
                        self.aliaslist.append(hostname)
                        hostname = answer.rdata.cname.__str__()

                if cnamefound is True:
                    return self.gethostbyname(hostname)
                else:
                    break
            elif response.authorities.__len__() > 0:
                # Loop through all the authorities
                for authority in response.authorities:
                    if authority.type_ == Type.NS:
                        # Store NS record of authoritative in cache
                        self.cache.add_record(authority)
                        servername = authority.rdata.nsdname
                        # Try to get an IP-address for the authoritative server from the additional records
                        for host in response.additionals:
                            if (host.type_ == Type.A) & (host.name == servername):
                                serverlist.insert(0, host.rdata.address)
                                # Store A record of authoritative server in cache
                                self.cache.add_record(host)



        # Get data
        if response:
            aliaslist, ipaddrlist = self.getresponselists(response.answers, hostname)
            # Store current state of cache in cache file
            self.cache.write_cache_file()

            return hostname, aliaslist, ipaddrlist
        else:
            return hostname, [], []


    def sendrequestmessage(self, sock, hostname, to):
        # Create and send query
        question = Question(hostname, Type.A, Class.IN)
        header = Header(9001, 0, 1, 0, 0, 0)
        header.qr = 0
        header.opcode = 0
        header.rd = 0   # No recursion desired
        query = Message(header, [question])
        sock.sendto(query.to_bytes(), (to, 53))

        # Receive response
        data = sock.recv(512)
        return Message.from_bytes(data)

    def getresponselists(self, answers, hostname):
        ipaddrlist = []
        for answer in answers:
            if answer.type_ == Type.A:
                ipaddrlist.append(answer.rdata.address)
            if answer.type_ == Type.CNAME:
                self.aliaslist.append(hostname.__str__())
                hostname = answer.rdata.cname
        return self.aliaslist, ipaddrlist

    def resolvehostnameincache(self, hostname):
        """
        Args:
            hostname (Name): domain name
        """
        cacheresult = self.cache.lookup(hostname, Type.A, Class.IN)
        if cacheresult.__len__() > 0:
            return cacheresult
        cacheresult = self.cache.lookup(hostname, Type.CNAME, Class.IN)
        if cacheresult.__len__() == 1:
            return cacheresult

        while True:
            if hostname.labels.__len__() == 0:
                return []
            cacheresult = self.cache.lookup(hostname, Type.NS, Class.IN)
            if cacheresult.__len__() > 0:
                result = []
                for record in cacheresult:
                    iprrs = self.cache.lookup(record.rdata.nsdname, Type.A, Class.IN)
                    result = result + [iprr.rdata.address for iprr in iprrs]
                return result
            else:
                hostname = hostname.supername()



