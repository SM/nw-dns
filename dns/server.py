#!/usr/bin/env python3

"""A recursive DNS server

This module provides a recursive DNS server. You will have to implement this
server using the algorithm described in section 4.3.2 of RFC 1034.
"""


from threading import Thread
from dns.zone import Zone
from dns.message import *
from dns.resolver import *
from dns.types import *
from dns.resource import *
from socket import *


class RequestHandler(Thread):
    """A handler for requests to the DNS server"""

    def __init__(self):
        """Initialize the handler thread"""
        super().__init__()
        self.daemon = True

    def run(self):
        """ Run the handler thread"""
        pass


class Server:
    """A recursive DNS server"""

    def __init__(self, port, caching, ttl):
        """Initialize the server

        Args:
            port (int): port that server is listening on
            caching (bool): server uses resolver with caching if true
            ttl (int): ttl for records (if > 0) of cache
        """
        self.caching = caching
        self.ttl = ttl
        self.port = port
        self.done = False

    def serve(self):
        """Start serving requests"""

        # Load master file
        zone = Zone()
        zone.read_master_file("dns\zone_file.txt")

        # Start socket and bind it to port
        serversocket = socket(AF_INET, SOCK_DGRAM)
        serversocket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        serversocket.bind(('', self.port))
        print("Socket up and running! Listening for requests.")

        resolver = Resolver(10, self.caching, self.ttl)

        while not self.done:
            # Get incoming request
            requestquery, returnTo = serversocket.recvfrom(512)
            print("Request received.")
            message = Message.from_bytes(requestquery)
            question = message.questions[0]
            qname = question.qname
            qtype = question.qtype

            # Create response
            responseHeader = Header(message.header.ident, 0b1000000010000000, 1, 0, 0, 0)
            response = Message(responseHeader, [question])

            #Search in zone for record
            while True:
                cname = False
                nameresult, zoneresult = zone.lookup(qname)
                if zoneresult.__len__() > 0:
                    if nameresult == qname:
                        # Records for this name exist in zone file
                        for record in zoneresult:
                            if record.type_ == Type.CNAME:
                                response.answers.append(record)
                                qname = record.rdata.cname
                                cname = True
                                break
                            elif record.type_ == qtype:
                                response.answers.append(record)
                            elif record.type_ == Type.NS:
                                response.authorities.append(record)

                            if record.type_ == Type.A:
                                response.header.aa = 1
                    else:
                        # Records exists for a supername of qname
                        for record in zoneresult:
                            if record.type_ == Type.CNAME:
                                response.answers.append(record)
                                qname = record.rdata.cname
                                cname = True
                                break
                            elif record.type_ == Type.NS:
                                response.authorities.append(record)

                    response = self.addauthoritiesarecords(response, zone)
                    if not cname:
                        # Send response unless a cname is encountered
                        self.sendresponse(serversocket, response, returnTo)
                        break

                else:
                    # Nothing found in zone file
                    if message.header.rd == 0:
                        responseHeader.rcode = 0b0011 # TODO: This error can be incorrect in some situations
                        self.sendresponse(serversocket, response, returnTo)
                    else:
                        # Use resolver to give an answer
                        response = self.getfromresolver(response, qname)
                        self.sendresponse(serversocket, response, returnTo)
                    break


    def shutdown(self):
        """Shut the server down"""
        self.done = True

    def sendresponse(self, socket, response, returnTo):
        """
        Args:
            response (Message): response to client
        """
        response.header.qd_count = response.questions.__len__()
        response.header.an_count = response.answers.__len__()
        response.header.ns_count = response.authorities.__len__()
        response.header.ar_count = response.additionals.__len__()
        socket.sendto(response.to_bytes(), returnTo)

    def addauthoritiesarecords(self, response, zone):
        for authority in response.authorities:
            nameresult, zoneresult = zone.lookupwithtype(authority.rdata.nsdname, Type.A)
            for record in zoneresult:
                response.additionals.append(record)

        return response

    def getfromresolver(self, response, qname):
        resolver = Resolver(10, self.caching, self.ttl)
        hostname, aliaslist, ipaddrlist = resolver.gethostbyname(qname.__str__())
        for alias in aliaslist:
            response.answers.append(ResourceRecord(Name(alias), Type.CNAME, Class.IN, self.ttl, CNAMERecordData(hostname)))
        for ipaddress in ipaddrlist:
            response.answers.append(ResourceRecord(Name(hostname), Type.A, Class.IN, self.ttl, ARecordData(ipaddress)))

        return response