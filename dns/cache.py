#!/usr/bin/env python3

"""A cache for resource records

This module contains a class which implements a cache for DNS resource records,
you still have to do most of the implementation. The module also provides a
class and a function for converting ResourceRecords from and to JSON strings.
It is highly recommended to use these.
"""


import json
import time

from dns.resource import ResourceRecord
from dns.name import Name


class RecordCache:
    """Cache for ResourceRecords"""

    def __init__(self, ttl):
        """Initialize the RecordCache

        Args:
            ttl (int): TTL of cached entries (if > 0)
        """
        self.records = {}
        self.ttl = ttl

    def lookup(self, dname, type_, class_):
        """Lookup resource records in cache

        Lookup for the resource records for a domain name with a specific type
        and class.

        Args:
            dname (str/Name): domain name
            type_ (Type): type
            class_ (Class): class
        """
        if not isinstance(dname, Name):
            dname = Name(dname)

        cachedata = self.records.get(dname.__str__())
        result = []
        if cachedata is None:
            return result
        else:
            for record in cachedata:
                if (record.type_ == type_) & (record.class_ == class_) & (record.ttl > time.time()):
                    result.append(record)

        return result


    def add_record(self, record):
        """Add a new Record to the cache

        Args:
            record (ResourceRecord): the record added to the cache
        """
        if self.ttl > 0:
            record.ttl = int(time.time()) + self.ttl
        else:
            record.ttl = int(time.time()) + record.ttl

        cachedata = self.records.get(record.name.__str__())
        if cachedata is None:
            # No record in the cache yet, so add records
            self.records[record.name.__str__()] = [record]
        else:
            # Record for this name already available in the cache
            # So TTL of existing record must be updated if matching record is found
            for cacherecord in cachedata:
                if cacherecord == record:
                    cacherecord.ttl = record.ttl
                    return
            # No matching record, so add new record
            self.records[record.name.__str__()].append(record)


    def read_cache_file(self):
        """Read the cache file from disk"""
        dcts = {}
        try:
            with open("cache", "r") as file_:
                dcts = json.load(file_)
        except:
            print("could not read cache")

        for k, v in dcts.items():
            self.records[k] = []
            for record in v:
                if record['ttl'] > time.time():
                    self.records[k].append(ResourceRecord.from_dict(record))
            if self.records[k].__len__() == 0:
                del self.records[k]

    def write_cache_file(self):
        """Write the cache file to disk"""
        dcts = {}
        for k, v in self.records.items():
            dcts[k] = []
            for record in v:
                if record.ttl > time.time():
                    dcts[k].append(ResourceRecord.to_dict(record))
            if dcts[k].__len__() == 0:
                del dcts[k]

        try:
            with open("cache", "w") as file_:
                json.dump(dcts, file_, indent=2)
        except:
            print("could not write cache")
